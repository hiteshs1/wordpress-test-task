<?php /* Template Name: Email Hosting Page  */ ?>

<body>
    <!--header section start-->
            <?php get_header(); ?>

    <!--header section end-->

    <div class="main">

        <!--hero section start-->
         <section class="ptb-120 gradient-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-6">
                        <div class="hero-content-wrap text-white position-relative">
                            <h1 class="text-white"><?php the_field('slider_title'); ?></h1>
                            <p class="lead"><?php the_field('slider_description'); ?></p>
                            <a href="<?php the_field('get_started_now_link'); ?>" class="btn btn-brand-03 btn-lg"><?php the_field('get_started_now_text'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="img-wrap">
                            <img src="<?php the_field('slider_image'); ?>" alt="email hosting" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--hero section end-->

        <!--feature section start-->
        <section id="features" class="feature-tab-section ptb-100 ">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 col-12 order-lg-last align-self-center">
                        <div class="image-box fadein text-xl-right text-center">
                            <img src="<?php the_field('advaced_search_image'); ?>" alt="wp-hosting" class="img-fluid" />
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-12 order-xl-first">
                        <h2><?php the_field('advaced_search_title'); ?></h2>
                        <p><?php the_field('advaced_search_desc'); ?></p>
                        <div class="row pt-2">
                            <?php 
                         if( have_rows('advanced_search_inner_point') ):
                            while( have_rows('advanced_search_inner_point') ) : the_row();
                        $advanced_search_inner_point_title = get_sub_field('advanced_search_inner_point_title');
                        ?>
                            <div class="col-md-6 col-12">
                                <h5><?php echo $advanced_search_inner_point_title; ?></h5>
                                <ul class="disc-style">
                             <?php 
                                    if( have_rows('advanced_search_inner_point_list') ):
                            while( have_rows('advanced_search_inner_point_list') ) : the_row();
                        $advanced_search_inner_all_points = get_sub_field('advanced_search_inner_all_points');
                        ?>
                                    <li><?php echo $advanced_search_inner_all_points; ?></li>
                            <?php 
                        endwhile;
                        else :
                        endif;
                        ?>     
                                </ul>
                            </div>
                         <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between mt-5">
                    <div class="col-xl-5 col-lg-5 col-12 order-lg-last align-self-center">
                        <div class="image-box fadein text-xl-right text-center">
                            <img src="<?php the_field('everything__you_need_image'); ?>" alt="wp-hosting" class="img-fluid" />
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-12 order-xl-last">
                         <h2><?php the_field('everything__you_need_title'); ?></h2>
                        <p><?php the_field('everything__you_need_desc'); ?></p>
                             <div class="row pt-2">
                            <?php 
                         if( have_rows('everything_you_need_inner_point') ):
                            while( have_rows('everything_you_need_inner_point') ) : the_row();
                        $everything_you_need_title = get_sub_field('everything_you_need_title');
                        ?>
                            <div class="col-md-6 col-12">
                                <h5><?php echo $everything_you_need_title; ?></h5>
                                <ul class="disc-style">
                             <?php 
                                    if( have_rows('everything_you_need_inner_list') ):
                            while( have_rows('everything_you_need_inner_list') ) : the_row();
                        $everything_you_need_inner_all_points = get_sub_field('everything_you_need_inner_all_points');
                        ?>
                                    <li><?php echo $everything_you_need_inner_all_points; ?></li>
                            <?php 
                        endwhile;
                        else :
                        endif;
                        ?>     
                                </ul>
                            </div>
                         <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                        </div>
                </div>
            </div>
        </section>
        <!--feature section end-->

        <!--call to action new section start-->
        <section class="ptb-60 primary-bg">
             <div class="container">
                <div class="row align-items-center justify-content-between">
                     <?php 
                         if( have_rows('expert_hosting_support') ):
                            while( have_rows('expert_hosting_support') ) : the_row();
                        $expert_hosting_title = get_sub_field('expert_hosting_title');
                        $expert_hosting_description = get_sub_field('expert_hosting_description');
                        $expert_email_id = get_sub_field('expert_email_id');
                        $expert_mobile_number = get_sub_field('expert_mobile_number');
                        $expert_image = get_sub_field('expert_image');

                      ?>
                    <div class="col-12 col-lg-6">
                        <div class="cta-content-wrap text-white">
                            <h2 class="text-white"><?php echo $expert_hosting_title; ?></h2>
                            <p><?php echo $expert_hosting_description; ?></p>
                        </div>
                        <div class="support-action d-inline-flex flex-wrap">
                            <a href="mailto:<?php echo $expert_email_id; ?>" class="mr-3"><i class="fas fa-comment mr-1 color-accent"></i> <span><?php echo $expert_email_id; ?></span></a>
                            <a href="tel:<?php echo $expert_mobile_number; ?>" class="mb-0"><i class="fas fa-phone-alt mr-1 color-accent"></i> <span><?php echo $expert_mobile_number; ?></span></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 d-none d-lg-block">
                        <div class="cta-img-wrap text-center">
                            <img src="<?php echo $expert_image; ?>" width="250" class="img-fluid" alt="server room">
                        </div>
                    </div>
                </div>
                <?php 
                 endwhile;
                 else :
                 endif;?>
            </div>
        </section>
        <!--call to action new section end-->

        <!--pricing section start-->
        <section class="pricing-section ptb-100 ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-4">
                            <h2><?php the_field('flexible_plan_title'); ?></h2>
                            <p>
                                <?php the_field('flexible_plan_desc'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-md-center justify-content-center">
                     <?php 
                         if( have_rows('plan_list') ):
                            while( have_rows('plan_list') ) : the_row();
                        $plan_image = get_sub_field('plan_image');
                        $plan_name = get_sub_field('plan_name');
                        $business_size = get_sub_field('business_size');
                        $starting_at_text = get_sub_field('starting_at_text');
                        $plan_price = get_sub_field('plan_price');
                        $plan_validity = get_sub_field('plan_validity');
                        $get_started_now_text = get_sub_field('get_started_now_text');
                        $get_started_now_button_link = get_sub_field('get_started_now_button_link');


                      ?>
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <div class="text-center bg-white single-pricing-pack-2 mt-4 rounded border">
                            <div class="pricing-icon">
                                <img src="<?php echo $plan_image; ?>" width="60" alt="hosing">
                            </div>
                            <h4 class="package-title"><?php echo $plan_name; ?></h4>
                            <p class="mb-4"><?php echo $business_size; ?></p>
                            <div class="pricing-price pt-4">
                                <small><?php echo $starting_at_text; ?></small>
                                <div class="h2"><?php echo $plan_price; ?> <span class="price-cycle h4">/<?php echo $plan_validity; ?></span></div>
                            </div>
                            <a href="<?php echo $get_started_now_button_link; ?> " class="btn btn-brand-01"><?php echo $get_started_now_text; ?> </a>
                        </div>
                    </div>
                    <?php 
                        endwhile;
                        else :
                        endif;?>
                    <div class="col-12">
                        <div class="support-cta text-center mt-5">
                            <h5 class="mb-1"><span class="ti-headphone-alt color-primary mr-3"></span><?php the_field('help_section_text'); ?>
                            </h5>
                            <?php the_field('help_section_desc'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--pricing section end-->

        <!--testimonial and review section start-->
        <section class="review-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="section-heading">
                            <h2><?php the_field('trusted_by_client_title'); ?></h2>
                            <p><?php the_field('trusted_by_client_description'); ?></p>
                            <a href="<?php the_field('write_about_us_link'); ?>" class="btn btn-outline-brand-01 btn-sm"><?php the_field('write_about_us_text'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <div class="owl-carousel owl-theme client-testimonial-2 dot-bottom-center custom-dot">
                            <?php 
                         if( have_rows('testimonial') ):
                            while( have_rows('testimonial') ) : the_row();
                        $short_testimonial = get_sub_field('short_testimonial');
                        $testimonial_full_description = get_sub_field('testimonial_full_description');
                        $client_image = get_sub_field('client_image');
                        $client_name = get_sub_field('client_name');
                        $client_post = get_sub_field('client_post');

                      ?>
                            <div class="item">
                                <div class="border single-review-wrap bg-white p-4 m-3">
                                    <div class="review-body">
                                        <h5><?php echo $short_testimonial; ?></h5>
                                        <p><?php echo $testimonial_full_description; ?></p>
                                    </div>
                                    <div class="review-author d-flex align-items-center">
                                        <div class="author-avatar">
                                            <img src="<?php echo $client_image; ?>" width="64" alt="author" class="rounded-circle shadow-sm img-fluid mr-3" />
                                            <span>“</span>
                                        </div>
                                        <div class="review-info">
                                            <h6 class="mb-0"><?php echo $client_name; ?></h6>
                                            <span><?php echo $client_post; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                             <?php 
                        endwhile;
                        else :
                        endif;?>  
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial and review section end-->

    </div>

    <!--footer section start-->
    <?php get_footer(); ?>
    <!--endbuild-->
</body>

