<?php /* Template Name: Wp Hosting Page  */ ?>

<body>

    <!--header section start-->
        <?php get_header(); ?>
    <!--header section end-->

    <div class="main">

        <!--hero section start-->
        <section class="ptb-120 gradient-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-6">
                        <div class="hero-content-wrap text-white position-relative">
                            <h1 class="text-white"><?php the_field('slider_title'); ?></h1>
                            <p class="lead"><?php the_field('slider_description'); ?></p>
                            <a href="<?php the_field('get_started_now_link'); ?>" class="btn btn-brand-03 btn-lg"><?php the_field('get_started_now_text'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="img-wrap">
                            <img src="<?php the_field('slider_image'); ?>" alt="email hosting" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--hero section end-->

        <!--operating system promo start-->
        <section class="appliction-hosting mt-n-65 ">
            <div class="container">
                <div class="d-none">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-5">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                       <div class="application-hosting-wrap">
                            <ul class="app-list operating-list">
                        <?php 
                         if( have_rows('available_operating_system_list') ):
                            while( have_rows('available_operating_system_list') ) : the_row();
                        $operating_system_name = get_sub_field('operating_system_name');
                        $operating_system_image = get_sub_field('operating_system_image');
                        ?>
    
                            <li><a href="#" class="bg-white shadow text-dark"><img src="<?php echo $operating_system_image; ?>" alt="icon"> <span><?php echo $operating_system_name; ?></span></a></li>
                        <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--operating system promo end-->

        <!--pricing section start-->
        <section class="wp-pricing-section ptb-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center">
                            <h2><?php the_field('high_performance_title'); ?></h2>
                            <p class="lead"><?php the_field('high_performance_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="wp-price-wrap" style="background: url('assets/img/wordpress-icon-color.svg')no-repeat center center">
                        <?php 
                         if( have_rows('hosting_plans') ):
                            while( have_rows('hosting_plans') ) : the_row();
                        $plan_name = get_sub_field('plan_name');
                        $plan_powered_by_image = get_sub_field('plan_powered_by_image');
                        $plan_start_from_text = get_sub_field('plan_start_from_text');
                        $plan_price = get_sub_field('plan_price');
                        $plan_validity = get_sub_field('plan_validity');
                        $buy_wordpress_hosting_text = get_sub_field('buy_wordpress_hosting_text');
                        $buy_wordpress_hosting_link = get_sub_field('buy_wordpress_hosting_link');

                        ?>
                        <div class="wp-single-price">
                            <div class="wp-price-head mb-4">
                                <h3 class="mb-1 h4"><?php echo $plan_name; ?></h3>
                                <img src="<?php echo $plan_powered_by_image; ?>" alt="power by cpanel" width="200" class="img-fluid">
                            </div>
                            <ul class="wp-price-feature-list">
                        <?php 
                         if( have_rows('plan_description_list') ):
                            while( have_rows('plan_description_list') ) : the_row();
                        $plan_list = get_sub_field('plan_list');

                        ?>
                                <li class="py-1"><i class="far fa-check fa-lg text-success mr-2"></i> <span><?php echo $plan_list; ?></span></li>
                        <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                            </ul>
                            <div class="wp-price-action mt-4">
                                <h4 class="h5 color-primary mb-0"><?php echo $plan_start_from_text; ?></h4>
                                <span class="h2 price d-block"><?php echo $plan_price; ?> <small>/<?php echo $plan_validity; ?></small></span>
                                <a href="<?php echo $buy_wordpress_hosting_link; ?>" class="btn btn-brand-01 btn-lg"><?php echo $buy_wordpress_hosting_text; ?></a>
                            </div>
                        </div>
                     <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <!--pricing section end-->

        <!--feature section start-->
        <section class="feature-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center">
                            <h2><?php the_field('best_hosting_feature_title'); ?></h2>
                            <p class="lead"><?php the_field('best_hosting_feature_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php 
                         if( have_rows('best_hosting_feature_list') ):
                            while( have_rows('best_hosting_feature_list') ) : the_row();
                        $best_hosting_feature_name = get_sub_field('best_hosting_feature_name');
                        $best_hosting_feature_description = get_sub_field('best_hosting_feature_description');
                      ?>
                    <div class="col-md-6 col-lg-4 mt-4">
                        <div class="features-box p-5 bg-white">
                            <div class="features-box-icon mb-3">
                                <span class="ti-panel icon-size-md color-primary"></span>
                            </div>
                            <div class="features-box-content">
                                <h5><?php echo $best_hosting_feature_name; ?></h5>
                                <p><?php echo $best_hosting_feature_description; ?></p>
                            </div>
                        </div>
                    </div>
                     <?php 
                        endwhile;
                        else :
                        endif;?>
                </div>
            </div>
        </section>
        <!--feature section end-->

        <!--call to action new section start-->
        <section class="ptb-60 primary-bg">
             <div class="container">
                <div class="row align-items-center justify-content-between">
                     <?php 
                         if( have_rows('expert_hosting_support') ):
                            while( have_rows('expert_hosting_support') ) : the_row();
                        $expert_hosting_title = get_sub_field('expert_hosting_title');
                        $expert_hosting_description = get_sub_field('expert_hosting_description');
                        $expert_email_id = get_sub_field('expert_email_id');
                        $expert_mobile_number = get_sub_field('expert_mobile_number');
                        $expert_image = get_sub_field('expert_image');

                      ?>
                    <div class="col-12 col-lg-6">
                        <div class="cta-content-wrap text-white">
                            <h2 class="text-white"><?php echo $expert_hosting_title; ?></h2>
                            <p><?php echo $expert_hosting_description; ?></p>
                        </div>
                        <div class="support-action d-inline-flex flex-wrap">
                            <a href="mailto:<?php echo $expert_email_id; ?>" class="mr-3"><i class="fas fa-comment mr-1 color-accent"></i> <span><?php echo $expert_email_id; ?></span></a>
                            <a href="tel:<?php echo $expert_mobile_number; ?>" class="mb-0"><i class="fas fa-phone-alt mr-1 color-accent"></i> <span><?php echo $expert_mobile_number; ?></span></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 d-none d-lg-block">
                        <div class="cta-img-wrap text-center">
                            <img src="<?php echo $expert_image; ?>" width="250" class="img-fluid" alt="server room">
                        </div>
                    </div>
                </div>
                <?php 
                        endwhile;
                        else :
                        endif;?>
            </div>
        </section>
        <!--call to action new section end-->

        <!--faq section start-->
        <section id="faq" class="ptb-100 ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-5">
                             <h2><?php the_field('faq_title'); ?></h2>
                            <p><?php the_field('faq_description'); ?></p>
                        </div>
                    </div>
                </div>
                    <div class="inr-leftsec">
             <?php 
                         $j=0;
                         if( have_rows('faq_list') ):
                            while( have_rows('faq_list') ) : the_row();
                        $faq_question = get_sub_field('faq_question');
                        $faq_answer = get_sub_field('faq_answer');
                        
                        ?>
                <div class="text">
                <a  class="menuitem submenuheader" href="#"><?php echo $faq_question; ?></a>    
                <div class="submenu"> <p><?php echo $faq_answer; ?></p> </div>      
                </div>
                      <?php 
                       $j++; endwhile;
                        else :
                        endif;?>  
           
            </div>
            </div>
        </section>
        <!--faq section end-->

        <!--testimonial and review section start-->
        <section class="review-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="section-heading">
                            <h2><?php the_field('trusted_by_client_title'); ?></h2>
                            <p><?php the_field('trusted_by_client_description'); ?></p>
                            <a href="<?php the_field('write_about_us_link'); ?>" class="btn btn-outline-brand-01 btn-sm"><?php the_field('write_about_us_text'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <div class="owl-carousel owl-theme client-testimonial-2 dot-bottom-center custom-dot">
                            <?php 
                         if( have_rows('testimonial') ):
                            while( have_rows('testimonial') ) : the_row();
                        $short_testimonial = get_sub_field('short_testimonial');
                        $testimonial_full_description = get_sub_field('testimonial_full_description');
                        $client_image = get_sub_field('client_image');
                        $client_name = get_sub_field('client_name');
                        $client_post = get_sub_field('client_post');

                      ?>
                            <div class="item">
                                <div class="border single-review-wrap bg-white p-4 m-3">
                                    <div class="review-body">
                                        <h5><?php echo $short_testimonial; ?></h5>
                                        <p><?php echo $testimonial_full_description; ?></p>
                                    </div>
                                    <div class="review-author d-flex align-items-center">
                                        <div class="author-avatar">
                                            <img src="<?php echo $client_image; ?>" width="64" alt="author" class="rounded-circle shadow-sm img-fluid mr-3" />
                                            <span>“</span>
                                        </div>
                                        <div class="review-info">
                                            <h6 class="mb-0"><?php echo $client_name; ?></h6>
                                            <span><?php echo $client_post; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                             <?php 
                        endwhile;
                        else :
                        endif;?>  
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial and review section end-->

    </div>

    <!--footer section start-->
        <?php get_footer(); ?>
    <!--endbuild-->
</body>

