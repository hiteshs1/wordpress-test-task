<?php /* Template Name: Shared Hosting Page  */ ?>

<body>

    <!--preloader start-->
    <?php get_header(); ?>
    <!--header section end-->

    <div class="main">

        <!--hero section start-->
        <section class="hero-slider-section">
            <div class="owl-carousel owl-theme hero-slider-one custom-dot dot-bottom-center">
                     <?php 
                         if( have_rows('slider') ):
                            while( have_rows('slider') ) : the_row();
                        $slider_image = get_sub_field('slider_image');
                        $slider_small_text = get_sub_field('slider_small_text');
                        $slider_main_text = get_sub_field('slider_main_text');
                        $feature_price_text = get_sub_field('feature_price_text');
                        $get_started_now_text = get_sub_field('get_started_now_text');
                        $get_started_now_button = get_sub_field('get_started_now_button');
                        $price_currency = get_sub_field('price_currency');
                        $offer_price = get_sub_field('offer_price');
                        $cent_value = get_sub_field('cent_value');
                        $per_value = get_sub_field('per_value');
                        $regular_price_text = get_sub_field('regular_price_text');
                        $regular_price = get_sub_field('regular_price');
                      ?>
                <div class="item">
                    <div class="bg-image hero-equal-height ptb-120" data-overlay="8">
                        <div class="background-image-wraper" style="background: url('<?php echo get_sub_field('slider_image'); ?>')no-repeat center center / cover; opacity: 1;"></div>
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-md-8 col-lg-7">
                                    <div class="hero-content-wrap text-white position-relative z-index">
                                        <span class="text-white h5 font-weight-normal"><?php echo $slider_small_text; ?></span>
                                        <h1 class="text-white"><?php echo $slider_main_text; ?></h1>
                                        <ul class="list-unstyled tech-feature-list">
                                            <?php 
                                            if( have_rows('slider_repeater_list') ):
                                             while( have_rows('slider_repeater_list') ) : the_row();
                                             $repeater_list = get_sub_field('repeater_list');
                                             ?>
                                            <li class="py-1"><span class="fas fa-check-circle color-accent mr-2"></span><?php echo $repeater_list; ?></li>
                                             <?php 
                                              endwhile;
                                              else :
                                              endif;?>

                                        </ul>
                                        <div class="action-btns mt-2">
                                            <p><?php echo $feature_price_text; ?></p>
                                            <a href="<?php echo $get_started_now_button; ?>" class="btn btn-brand-03 mr-3"><?php echo $get_started_now_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 d-none d-md-none d-lg-block">
                                    <div class="big-price-wrap position-relative">
                                        <div class="price-block d-flex justify-content-center text-white">
                                            <span class="price-currency"><?php echo $price_currency; ?></span>
                                            <div class="offer-price"><?php echo $offer_price; ?></div>
                                            <div class="right-block d-flex">
                                                <span class="cent-value"><?php echo $cent_value; ?></span>
                                                <span class="per-value"><?php echo $per_value; ?></span>
                                            </div>
                                        </div>
                                        <span class="d-block text-white regular-price text-center"><?php echo $regular_price_text; ?> <?php echo $regular_price; ?></span>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                  <?php 
 

              endwhile;
              else :
              endif;?>
            
            </div>
        </section>
        <!--hero section end-->

        <!--pricing section start-->
        <section id="pricing" class="pricing-section ptb-100 ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-4">
                            <h2><?php the_field('hosting_plan_main_title'); ?></h2>
                            <p class="lead">
                                <?php the_field('hosting_plan_description'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-md-center justify-content-center">
                        <?php 
                         if( have_rows('plan_price') ):
                            while( have_rows('plan_price') ) : the_row();
                        $plan_amount = get_sub_field('plan_amount');
                        $plan_subscription_slot = get_sub_field('plan_subscription_slot');
                        $plan_name = get_sub_field('plan_name');
                        $plan_description = get_sub_field('plan_description');
                        $disk = get_sub_field('disk');
                        $disk_more_details = get_sub_field('disk_more_details');
                        $bandwidth = get_sub_field('bandwidth');
                        $website_host = get_sub_field('website_host');
                        $email_accounts = get_sub_field('email_accounts');
                        $email_account_details = get_sub_field('email_account_details');
                        $free_domain_name = get_sub_field('free_domain_name');
                        $domain_more_details = get_sub_field('domain_more_details');
                        $technical_support = get_sub_field('technical_support');
                        $uptime_gurantee = get_sub_field('uptime_gurantee');
                        $purchase_now_text = get_sub_field('purchase_now_text');
                        $purchase_now_button_link = get_sub_field('purchase_now_button_link');
                        $sapcification_text = get_sub_field('sapcification_text');
                        $spacification_link = get_sub_field('spacification_link');

                      ?>
                    <div class="col-lg-4 col-md-6 col-sm-8">
                        <div class="popular-price bg-white pricing-new-wrapper mt-4 p-5 shadow-sm border">
                            <div class="pricing-price d-flex justify-content-between align-items-center pb-4">
                                <span class="p-icon"><i class="fad fa-rocket color-secondary"></i></span>
                                <div class="h2 mb-0 text-dark"><?php echo $plan_amount; ?><span class="price-cycle h5">/<?php echo $plan_subscription_slot; ?></span></div>
                            </div>
                            <div class="pricing-info">
                                <h5><?php echo $plan_name; ?></h5>
                                <p class="text-muted"><?php echo $plan_description; ?></p>
                            </div>
                            <div class="pricing-content">
                                <ul class="list-unstyled pricing-feature-list-2 mt-4">
                                    <li><i class="far fa-hdd mr-2"></i>
                                        <p><?php echo $disk; ?></p>
                                        <span class="text-under-line" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $disk_more_details; ?>"><i class="fad fa-info-circle ml-1"></i></span>
                                    </li>
                                    <li><i class="far fa-router mr-2"></i>
                                        <p><?php echo $bandwidth; ?></p>
                                    </li>
                                    <li><i class="far fa-browser mr-2"></i>
                                        <p><?php echo $website_host; ?></p>
                                    </li>
                                    <li><i class="far fa-mail-bulk mr-2"></i>
                                        <p><?php echo $email_accounts; ?></p>
                                        <span class="text-under-line" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $email_account_details; ?>"><i class="fad fa-info-circle ml-1"></i></span>
                                    </li>

                                    <li><i class="far fa-globe mr-2"></i>
                                        <p><?php echo $free_domain_name; ?></p>
                                        <span class="text-under-line" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $domain_more_details; ?>"><i class="fad fa-info-circle ml-1"></i></span>
                                    </li>

                                    <li><i class="far fa-headset mr-2"></i>
                                        <p><?php echo $technical_support; ?></p>
                                    </li>

                                    <li><i class="far fa-upload mr-2"></i>
                                        <p><?php echo $uptime_gurantee; ?></p>
                                    </li>
                                </ul>
                                <a href="<?php echo $spacification_link; ?>" class="mb-4 d-block"><?php echo $sapcification_text; ?><i class="far fa-arrow-right pl-2"></i></a>
                                <a href="<?php echo $purchase_now_button_link; ?>" class="btn btn-outline-brand-01 btn-block" target="_blank"><?php echo $purchase_now_text; ?></a>

                            </div>
                        </div>
                    </div>
                     <?php 
                    endwhile;
                    else :
                    endif;?>
            
                   <?php 
                         if( have_rows('compare_plan') ):
                            while( have_rows('compare_plan') ) : the_row();
                        $compare_text = get_sub_field('compare_text');
                        $compare_plan_links = get_sub_field('compare_plan_links');
                      ?>
    
                    <div class="col-12">
                        <div class="support-cta text-center mt-5">
                            <a href="<?php echo $compare_plan_links; ?>" target="_blank">
                                <h5 class="mb-1"><span class="ti-loop color-primary mr-2"></span><?php echo $compare_text; ?>
                                </h5>
                            </a>
                        </div>
                    </div>
                  <?php 
                    endwhile;
                    else :
                    endif;?>
                </div>
            </div>
        </section>
        <!--pricing section end-->

        <!--operating system promo start-->
        <section class="appliction-hosting ptb-100 gray-light-bg">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-5">
                            <h2><?php the_field('available_operating_system_text'); ?></h2>
                            <p class="lead"><?php the_field('available_operating_system_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="application-hosting-wrap">
                            <ul class="app-list operating-list">
                        <?php 
                         if( have_rows('available_operating_system_list') ):
                            while( have_rows('available_operating_system_list') ) : the_row();
                        $operating_system_name = get_sub_field('operating_system_name');
                        $operating_system_image = get_sub_field('operating_system_image');
                        ?>
    
                            <li><a href="#" class="bg-white shadow text-dark"><img src="<?php echo $operating_system_image; ?>" alt="icon"> <span><?php echo $operating_system_name; ?></span></a></li>
                        <?php 
                        endwhile;
                        else :
                        endif;
                        ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--operating system promo end-->

        <!--feature section start-->
        <section class="feature-section ptb-100 ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center">
                            <h2><?php the_field('best_hosting_feature_title'); ?></h2>
                            <p class="lead"><?php the_field('best_hosting_feature_description'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                     <?php 
                         if( have_rows('best_hosting_feature_list') ):
                            while( have_rows('best_hosting_feature_list') ) : the_row();
                        $best_hosting_feature_name = get_sub_field('best_hosting_feature_name');
                        $best_hosting_feature_description = get_sub_field('best_hosting_feature_description');
                      ?>
                    <div class="col-md-6 col-lg-4 mt-4">
                        <div class="features-box p-5 ">
                            <div class="features-box-icon mb-3">
                                <span class="ti-panel icon-size-md color-primary"></span>
                            </div>
                            <div class="features-box-content">
                                <h5><?php echo $best_hosting_feature_name; ?></h5>
                                <p><?php echo $best_hosting_feature_description; ?></p>
                            </div>
                        </div>
                    </div>
                   <?php 
                        endwhile;
                        else :
                        endif;?>
                </div>
            </div>
        </section>
        <!--feature section end-->

        <!--call to action new section start-->
        <section class="ptb-60 primary-bg">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                     <?php 
                         if( have_rows('expert_hosting_support') ):
                            while( have_rows('expert_hosting_support') ) : the_row();
                        $expert_hosting_title = get_sub_field('expert_hosting_title');
                        $expert_hosting_description = get_sub_field('expert_hosting_description');
                        $expert_email_id = get_sub_field('expert_email_id');
                        $expert_mobile_number = get_sub_field('expert_mobile_number');
                        $expert_image = get_sub_field('expert_image');

                      ?>
                    <div class="col-12 col-lg-6">
                        <div class="cta-content-wrap text-white">
                            <h2 class="text-white"><?php echo $expert_hosting_title; ?></h2>
                            <p><?php echo $expert_hosting_description; ?></p>
                        </div>
                        <div class="support-action d-inline-flex flex-wrap">
                            <a href="mailto:<?php echo $expert_email_id; ?>" class="mr-3"><i class="fas fa-comment mr-1 color-accent"></i> <span><?php echo $expert_email_id; ?></span></a>
                            <a href="tel:<?php echo $expert_mobile_number; ?>" class="mb-0"><i class="fas fa-phone-alt mr-1 color-accent"></i> <span><?php echo $expert_mobile_number; ?></span></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 d-none d-lg-block">
                        <div class="cta-img-wrap text-center">
                            <img src="<?php echo $expert_image; ?>" width="250" class="img-fluid" alt="server room">
                        </div>
                    </div>
                </div>
                <?php 
                        endwhile;
                        else :
                        endif;?>
            </div>
        </section>
        <!--call to action new section end-->

        <!--faq section start-->
        <section id="faq" class="ptb-100 ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading text-center mb-5">
                            <h2><?php the_field('faq_title'); ?></h2>
                            <p><?php the_field('faq_description'); ?></p>
                        </div>
                    </div>
                </div>

        <div class="inr-leftsec">
             <?php 
                         $j=0;
                         if( have_rows('faq_list') ):
                            while( have_rows('faq_list') ) : the_row();
                        $faq_question = get_sub_field('faq_question');
                        $faq_answer = get_sub_field('faq_answer');
                        
                        ?>
                <div class="text">
                <a  class="menuitem submenuheader" href="#"><?php echo $faq_question; ?></a>    
                <div class="submenu"> <p><?php echo $faq_answer; ?></p> </div>      
                </div>
                      <?php 
                       $j++; endwhile;
                        else :
                        endif;?>  
           
            </div>
        </section>
        <!--faq section end-->

        <!--testimonial and review section start-->
        <section class="review-section ptb-100 gray-light-bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-4">
                        <div class="section-heading">
                            <h2><?php the_field('trusted_by_client_title'); ?></h2>
                            <p><?php the_field('trusted_by_client_description'); ?></p>
                            <a href="<?php the_field('write_about_us_link'); ?>" class="btn btn-outline-brand-01 btn-sm"><?php the_field('write_about_us_text'); ?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <div class="owl-carousel owl-theme client-testimonial-2 dot-bottom-center custom-dot">
                            <?php 
                         if( have_rows('testimonial') ):
                            while( have_rows('testimonial') ) : the_row();
                        $short_testimonial = get_sub_field('short_testimonial');
                        $testimonial_full_description = get_sub_field('testimonial_full_description');
                        $client_image = get_sub_field('client_image');
                        $client_name = get_sub_field('client_name');
                        $client_post = get_sub_field('client_post');

                      ?>
                            <div class="item">
                                <div class="border single-review-wrap bg-white p-4 m-3">
                                    <div class="review-body">
                                        <h5><?php echo $short_testimonial; ?></h5>
                                        <p><?php echo $testimonial_full_description; ?></p>
                                    </div>
                                    <div class="review-author d-flex align-items-center">
                                        <div class="author-avatar">
                                            <img src="<?php echo $client_image; ?>" width="64" alt="author" class="rounded-circle shadow-sm img-fluid mr-3" />
                                            <span>“</span>
                                        </div>
                                        <div class="review-info">
                                            <h6 class="mb-0"><?php echo $client_name; ?></h6>
                                            <span><?php echo $client_post; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                             <?php 
                        endwhile;
                        else :
                        endif;?>  
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial and review section end-->

    </div>

    <!--footer section start-->
    <?php get_footer(); ?>
    <!--endbuild-->
</body>

</html>